import React, { Component } from 'react';
import Lottery from './Lottery'
import { rand } from './random'
import './App.css';


class App extends Component {

    numbersFrom5to36 = [];

    state = { randomNumbers: [] };

    generateNumbersFrom5to36 = () => {
        for (let i = 5; i <= 36; i++) { 
            this.numbersFrom5to36.push(i); 
        }
    }

    getRandomNumbers = () => {
        let numbers = [...this.numbersFrom5to36];
        let randomNumbers = [];
        for (let i = 0; i < 5; i++) {
            let randomIndex = rand(0, numbers.length);
            randomNumbers.push(numbers[randomIndex]);
            numbers.splice(randomIndex, 1);
        }
        this.setState({randomNumbers: randomNumbers.sort((a, b) => a > b)});
    };

    componentDidMount = () => {
        this.generateNumbersFrom5to36();
        this.getRandomNumbers();
    };

    handleClick = () => this.getRandomNumbers();

    render = () => {
        return (
            <div>
                <button className="button" onClick={this.handleClick}>New numbers</button>
                <div className="numbers">
                    {this.state.randomNumbers.map((number, index) => <Lottery number={number} key={index} />)}
                </div>
            </div>
        );
    }
}

export default App;
