import React from 'react';

const Lottery = props => {
    return (
      <div className="lottery_number">{props.number}</div>
    );
};

export default Lottery;